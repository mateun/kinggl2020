// KingGL_2020.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <display.h>
#include <glrenderer.h>
#include <glm/glm.hpp>



#include "camera.h"
#include "gmath.h"
#include "importer.h"
#include "model.h"

#undef main

using namespace glm;

GLuint createShaderProg(ggraphics::GLRenderer& glRenderer, GLuint oldProg)
{
	if (oldProg) glDeleteProgram(oldProg);
	GLuint vertexShader = glRenderer.createVertexShader("E:\\Projects\\C++\\KingGL_2020\\shaders\\vshader.glsl");
	GLuint fragmentShader = glRenderer.createFragmentShader("E:\\Projects\\C++\\KingGL_2020\\shaders\\fshader.glsl");
	return glRenderer.createProgram(vertexShader, fragmentShader);
}

struct RenderContext
{
	GLuint shaderProg;
	GLuint vao;
	ggraphics::GLRenderer glRenderer;
	ggraphics::Camera camera;
};

bool handleEvents(ggraphics::Camera&);

int main()
{
    ggraphics::Display display(800, 600, false);
	ggraphics::GLRenderer glRenderer(display);
	GLuint shaderProg = createShaderProg(glRenderer, 0);

	ggraphics::Camera camera(vec3(0, 35, 45), vec3(0, 0, 0), vec3(0, 1, 0), 0.62);
		;

	ggraphics::Texture dummyTexture = ggraphics::Texture("E:/Pictures/3DModels/objects/fullscreen.bmp", vec3(0));
    ggraphics::Model model = importFromFile("E:/Pictures/3DModels/objects/double_mat.obj", glRenderer);
	for (auto& mesh : model.meshes)
	{
		mesh.material->shaderProg = shaderProg;
	}

	ggraphics::Model ground = importFromFile("E:/Pictures/3DModels/objects/ground.obj", glRenderer);
	for (auto& mesh : ground.meshes)
	{
		mesh.material->shaderProg = shaderProg;
	}


	GLuint miniMapColTex;
	GLuint miniMapDepthTex;
	GLuint miniMapFB = glRenderer.createFramebuffer(800, 600, &miniMapColTex, &miniMapDepthTex);

	ggraphics::Camera miniMapCamera(vec3(0, 100, 0.1), vec3(0, 0, -0.1), vec3(0, 1, 0), .62f);
	
	
	
	bool gameRunning = true;
	while (gameRunning)
	{
		static float angle = 0;
		static float angleBunny1 = 0;
		static float angleBunny2 = 0;
		gameRunning = handleEvents(camera);


		
		glRenderer.clearBackbuffer(0.11, 0.1, 0.1, 1);
		GLint lit = 1;
		model.worldPosition = vec3(-10, 0, -10);
		model.render(glRenderer, camera, &lit);

		model.worldPosition = vec3(30, 0, -11);
		model.render(glRenderer, camera, &lit);

		ground.worldScale = vec3(5, 1, 5);
		ground.render(glRenderer, camera, &lit);


		glBindFramebuffer(GL_FRAMEBUFFER, miniMapFB);
		glRenderer.clearBackbuffer(0.4, 0.1, 0.1, 1);
		lit = 0;
		model.worldPosition = vec3(-10, 0, -10);
		model.render(glRenderer, miniMapCamera, &lit);

		model.worldPosition = vec3(30, 0, -11);
		model.render(glRenderer, miniMapCamera, &lit);

		ground.render(glRenderer, miniMapCamera, &lit);


		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glDisable((GL_DEPTH_TEST));
		glViewport(800-200, 600-200, 200, 180);
		glRenderer.drawFullScreenQuad(shaderProg, miniMapColTex);
		glEnable((GL_DEPTH_TEST));
		glViewport(0, 0, 800, 600);
		
		
		
		glRenderer.present();
	}


	
}

bool handleEvents(ggraphics::Camera& camera)
{
	const Uint8* kbState = SDL_GetKeyboardState(NULL);
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
		{
			return false;
		}

		if (event.type == SDL_KEYUP)
		{
			if (event.key.keysym.sym == SDLK_F6) {
				SDL_Log("recompile shader");
				//renderContext.shaderProg = createShaderProg(renderContext.glRenderer, renderContext.shaderProg);
			}

			if (event.key.keysym.sym == SDLK_F7) {
				SDL_Log("ray triangle test");
				SDL_Log("%d", rayIntersectsTriangle(vec3(-0.5f, -0.5f, -5), vec3(0.5f, -0.5f, -5),
				                                    vec3(-0.5f, 0.5f, -5), vec3(0, 0, -1), vec3(0.4f, .4f, 10)));
			}
		}

		
	}

	SDL_CaptureMouse(SDL_FALSE);
	int mouseX;
	int mouseY;
	SDL_GetMouseState(&mouseX, &mouseY);

	if (kbState[SDL_SCANCODE_A] || kbState[SDL_SCANCODE_LEFT] || mouseX < 50)
	{
		camera.strafeH(true);
	}

	if (kbState[SDL_SCANCODE_D] || kbState[SDL_SCANCODE_RIGHT] || mouseX > 800 - 50)
	{
		camera.strafeH(false);
	}

	if (kbState[SDL_SCANCODE_W] || kbState[SDL_SCANCODE_UP] || mouseY < 50 )
	{
		camera.strafeV(true);
	}

	if (kbState[SDL_SCANCODE_S] || kbState[SDL_SCANCODE_DOWN] || mouseY  > 600 - 50)
	{
		camera.strafeV(false);
	}

	if (kbState[SDL_SCANCODE_U])
	{
		camera.zoom(true);
	}


	if (kbState[SDL_SCANCODE_I])
	{
		camera.zoom(false);
	}

	if (kbState[SDL_SCANCODE_E])
	{
		camera.rotate(false);
	}

	if (kbState[SDL_SCANCODE_Q])
	{
		camera.rotate(true);
	}
	
	return true;
}
